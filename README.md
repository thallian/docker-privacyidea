[privacyIDEA](https://www.privacyidea.org/) server for two factor authentication.

Using PostgreSQL as database backend.

# Volumes
- `/etc/privacyidea/keys`

# Environment Variables
## ADMIN_PASSWORD
The admin password.

## SECRET_KEY
Key to encrypt the auth_token.

## PASSWORD_KEY
Key to encrypt the passwords.

## DB_HOST
Database host.

## DB_PORT
- default: 5432

Database port.

## DB_NAME
Database name.

## DB_USER
Database user.

## DB_PASSWORD
Password for the database user.

# Ports
- 5000
