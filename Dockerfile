FROM registry.gitlab.com/thallian/docker-confd-env:master

ENV PRIVACYIDEA_DIR /home/privacyidea

RUN addgroup -g 2222 privacyidea
RUN adduser -h ${PRIVACYIDEA_DIR} -S -D -u 2222 -G privacyidea privacyidea

RUN apk add --no-cache \
    expect \
    gnupg \
    python2 \
    py2-pip \
    py2-cffi \
    py2-pillow \
    py2-bcrypt \
    py2-cryptography \
    py2-openssl \
    py2-crypto \
    py2-lxml \
    py-psycopg2 \
    uwsgi \
    uwsgi-http \
    uwsgi-python

RUN pip2 install privacyidea

RUN mkdir -p /etc/privacyidea/keys
RUN chown -R privacyidea:privacyidea /etc/privacyidea/

ADD /rootfs /

EXPOSE 5000

VOLUME /etc/privacyidea/keys
